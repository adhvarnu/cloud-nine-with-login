package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
/* Nupur 991518688 */
public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular() {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Nupur12a" ) );
	}
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Nupur12" ) );
	}
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nupur1" ) );
	}
	@Test
	public void testIsValidLoginException( ) {
		
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "nupu" ) );
	}
		
	@Test
	public void testIsValidLogincharacterRegular( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Nup12a" ) );
	}
	
	@Test
	public void testIsValidLogincharacterException() {
		
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1-nupur" ) );
	}
	
	@Test
	public void testIsValidLogincharacterBoundaryOut( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nu1234" ) );
	}
	@Test
	public void testIsValidLogincharacterBoundaryIn( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nupur12" ) );
	}


}

package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {
	
	private static int MIN_LENGTH = 6;
	private String loginName;

	public static boolean isValidLoginName( String loginName ) {
		int count = 0;
		
		if(Character.isDigit(loginName.charAt(0))) {
			return false;
		}
		
		
		
		for(int i = 1 ; i<loginName.length(); i++) {
			
			if(Character.isDigit(loginName.charAt(i))) {
				return true;
			}
			}
	
		return(loginName !=null && !loginName.contains(" ") && loginName.length() >= MIN_LENGTH);
		
		
	}
}
